import "./Home.css";
import React from "react";

export const Home = () => {
  console.log("Home");
  return (
    <>
      <h1>Home page</h1>
      <div>
        Lorem, ipsum dolor sit amet consectetur adipisicing elit. Possimus ipsam
        dolore, perspiciatis corrupti, vitae accusantium blanditiis ab ipsa
        voluptates quidem sapiente quos optio eos molestiae similique,
        architecto ut libero. Explicabo.
      </div>
    </>
  );
};
