import "./Profile.css";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { userActions } from "../../store/actions";
import { Card, ListGroup } from "react-bootstrap";

export const Profile = () => {
  const dispatch = useDispatch();
  const { data } = useSelector(({ user }) => user);
  // console.log(data);
  console.log("Profile");

  useEffect(() => {
    dispatch(userActions.getProfile());
  }, [dispatch]);

  return (
    <>
      <h1>Profile page</h1>
      <Card>
        <Card.Header>Profile</Card.Header>
        <ListGroup variant="flush">
          <ListGroup.Item>{`Email: ${data.email}`}</ListGroup.Item>
          <ListGroup.Item>{`Firstname: ${data.firstName}`}</ListGroup.Item>
          <ListGroup.Item>{`Lastname: ${data.lastName}`}</ListGroup.Item>
          <ListGroup.Item>{`Created at: ${data.createdAt}`}</ListGroup.Item>
          <ListGroup.Item>{`Updated at: ${data.updatedAt}`}</ListGroup.Item>
        </ListGroup>
      </Card>
    </>
  );
};
