import "./SignIn.css";
import React from "react";
import { AuthForm } from "../../components";

export const SignIn = () => {
  console.log("SignIn");
  return (
    <div className="form-page d-flex flex-column align-items-center">
      <h1 className="text-center">Sign In page</h1>
      <AuthForm isLogin />
    </div>
  );
};
