import "./Catalog.css";
import React, { useEffect, useState } from "react";
import { useDispatch, useSelector } from "react-redux";
import { CardDeck, Row, Col } from "react-bootstrap";
import queryString from "query-string";
import { Card, Pagination, Spinner } from "../../components";
import { cardActions } from "../../store/actions";

export const Catalog = (props) => {
  console.log("Catalog");
  const { location, history } = props;
  const queryParams = queryString.parse(location.search);
  const [page, setPage] = useState(+queryParams.page - 1 || 0);
  const [limit] = useState(+queryParams.limit || 9);
  const dispatch = useDispatch();
  const { elements, totalCount } = useSelector(({ cards }) => cards);
  const isLoading = useSelector(({ ui }) => ui.isLoading);
  // const { elements, totalCount, isLoggedIn } = useSelector((state) => {
  //   return { ...state.cards, isLoggedIn: state.user.isLoggedIn };
  // });
  const pageCount = Math.ceil(totalCount / limit);
  const elementList = elements.map((el) => (
    <Col lg={4} key={el.id} className="mb-3">
      <Card {...el} />
    </Col>
  ));
  
  const handlePageClick = ({ selected }) => {
    setPage(selected);
    const search = queryString.stringify({
      ...queryParams,
      page: selected + 1,
    });
    history.push(`${location.pathname}?${search}`);
  };
  
  useEffect(() => {
    dispatch(cardActions.getAll({ limit, page: page + 1 }));
  }, [dispatch, page, limit, queryParams.page]);
  
  console.log("isLoading", isLoading);
  if (isLoading) {
    return <Spinner />;
  }
  
  return (
    <>
      <Row className="justify-content-sm-center">
        <h1>Catalog</h1>
      </Row>
      <CardDeck>{elementList}</CardDeck>
      <Row className="justify-content-sm-center">
        <Pagination
          pageCount={pageCount}
          currentPage={page}
          onPageChange={handlePageClick}
        />
      </Row>
    </>
  );
};
