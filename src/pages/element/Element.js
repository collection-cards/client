import "./Element.css";
import React, { useEffect } from "react";
import { useDispatch, useSelector } from "react-redux";
import { Card, Spinner } from "../../components";
import { cardActions } from "../../store/actions";

export const Element = (props) => {
  console.log("Element");
  const dispatch = useDispatch();
  const element = useSelector(({ cards }) => cards.elements[0]);
  const isLoading = useSelector(({ ui }) => ui.isLoading);

  useEffect(() => {
    dispatch(cardActions.getOneById(props.match.params.elementId));
  }, [dispatch, props.match.params.elementId]);

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <>
      <h1>Element page</h1>
      <Card {...element} />
    </>
  );
};
