import "./SignUp.css";
import React from "react";
import { AuthForm } from "../../components";

export const SignUp = () => {
  console.log("SignUp");
  return (
    <div className="form-page d-flex flex-column align-items-center">
      <h1 className="text-center">Sign Up page</h1>
      <AuthForm />
    </div>
  );
};
