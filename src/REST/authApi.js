import axios from "axios";
import { urls } from "./urls";
const url = process.env.REACT_APP_PUBLIC_URL; // http://localhost:8080

export const api = {
  authorization: async (token) => {
    const result = await axios.get(`${url}${urls.auth}`, {
      headers: { Authorization: token },
    });
    return result.data;
  },

  login: async (data = {}) => {
    const result = await axios.post(`${url}${urls.auth}`, { ...data });
    return result.data;
  },
};
