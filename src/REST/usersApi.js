import axios from "axios";
import { urls } from "./urls";
const url = process.env.REACT_APP_PUBLIC_URL; // http://localhost:8080

export const api = {
  create: async (data = {}) => {
    const response = await axios.post(`${url}${urls.users}`, { ...data });
    const user = response.data;
    return user;
  },

  getAll: async (params = {}) => {
    const response = await axios.get(`${url}${urls.users}`, { params });
    const elements = response.data.list;
    const totalCount = response.data.totalCount;
    return { elements, totalCount };
  },

  getOne: async (id, token) => {
    const { data } = await axios.get(`${url}${urls.users}/${id}`, {
      headers: { Authorization: token },
    });
    return { data };
  },

  getProfile: async (token) => {
    const { data } = await axios.get(`${url}${urls.users}${urls.profile}`, {
      headers: { Authorization: token },
    });
    return { data };
  },
};
