export const urls = {
  home: "/",
  cards: "/cards",
  users: "/users",
  auth: "/authorization",
  profile: "/profile",
};
