import axios from "axios";
import { urls } from "./urls";
const url = process.env.REACT_APP_PUBLIC_URL; // http://localhost:8080

export const api = {
  getAll: async (params = {}) => {
    const result = await axios.get(`${url}${urls.cards}`, { params });
    const elements = result.data.list;
    const totalCount = result.data.totalCount;
    return { elements, totalCount };
  },

  getOneById: async (id) => {
    const response = await axios.get(`${url}${urls.cards}/${id}`);
    return { elements: [response.data], totalCount: 1 };
  },
};
