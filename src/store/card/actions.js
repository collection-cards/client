import { types as actionTypes } from "./types";
import { cardsApi } from "../../REST";
import { uiActions } from "../actions";

export const actions = {
  getAll: (params = {}) => async (dispatch) => {
    dispatch(uiActions.startLoading());
    const response = await cardsApi.getAll(params);
    const action = {
      type: actionTypes.GET_ALL,
      payload: response,
    };
    dispatch(action);
    dispatch(uiActions.stopLoading());
  },
  
  getOneById: (id) => async (dispatch) => {
    dispatch(uiActions.startLoading());
    const element = await cardsApi.getOneById(id);
    const action = {
      type: actionTypes.GET_ONE_BY_ID,
      payload: { ...element },
    };
    dispatch(action);
    dispatch(uiActions.stopLoading());
  },
};
