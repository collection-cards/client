import { types as actionTypes } from "./types";

const initialState = {
  elements: [],
  totalCount: 0,
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.GET_ALL:
      return { ...state, ...action.payload };
    case actionTypes.GET_ONE_BY_ID:
      return { ...state, ...action.payload };
    default:
      return state;
  }
};
