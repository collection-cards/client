import { combineReducers, createStore, applyMiddleware } from "redux";
import { composeWithDevTools } from "redux-devtools-extension";
import thunk from "redux-thunk";
import { reducer as cards } from "./card/reducer";
import { reducer as user } from "./user/reducer";
import { reducer as ui } from "./ui/reducer";

const reducers = combineReducers({
  cards,
  user,
  ui,
});

export const store = createStore(
  reducers,
  composeWithDevTools(applyMiddleware(thunk))
);
