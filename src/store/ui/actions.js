import { types as actionTypes } from "./types";

export const actions = {
  startLoading: () => (dispatch) => {
    const action = { type: actionTypes.START_LOADING };
    dispatch(action);
  },

  stopLoading: () => (dispatch) => {
    const action = { type: actionTypes.STOP_LOADING };
    dispatch(action);
  },
};
