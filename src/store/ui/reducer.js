import { types as actionTypes } from "./types";

const initialState = {
  isLoading: false,
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.START_LOADING:
      return { ...state, isLoading: true };
    case actionTypes.STOP_LOADING:
      return { ...state, isLoading: false };
    default:
      return state;
  }
};
