import { types as actionTypes } from "./types";

const initialState = {
  data: null,
  isLoggedIn: null,
};

export const reducer = (state = initialState, action) => {
  switch (action.type) {
    case actionTypes.FILL_USER_DATA:
      return { ...state, data: action.payload, isLoggedIn: true };
    case actionTypes.LOGOUT:
      return { ...state, data: null, isLoggedIn: false };
    case actionTypes.SET_UNAUTHORIZED:
      return { ...state, isLoggedIn: false };
    default:
      return state;
  }
};
