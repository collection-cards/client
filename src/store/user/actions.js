import { types as actionTypes } from "./types";
import { usersApi, authApi } from "../../REST";
import { uiActions } from "../actions";

export const actions = {
  create: (data = {}) => async (dispatch) => {
    dispatch(uiActions.startLoading());
    const response = await usersApi.create(data);
    localStorage.setItem("token", response.token);
    const action = {
      type: actionTypes.FILL_USER_DATA,
      payload: response.user,
    };
    dispatch(action);
    dispatch(uiActions.stopLoading());
  },

  login: (data = {}) => async (dispatch) => {
    dispatch(uiActions.startLoading());
    const response = await authApi.login(data);
    localStorage.setItem("token", response.token);
    const action = {
      type: actionTypes.FILL_USER_DATA,
      payload: { ...response.user },
    };
    dispatch(action);
    dispatch(uiActions.stopLoading());
  },

  logout: () => async (dispatch) => {
    dispatch(uiActions.startLoading());
    localStorage.removeItem("token");
    const action = { type: actionTypes.LOGOUT };
    dispatch(action);
    dispatch(uiActions.stopLoading());
  },

  authorization: () => async (dispatch) => {
    dispatch(uiActions.startLoading());
    const token = localStorage.getItem("token");
    if (!token) {
      dispatch({ type: actionTypes.SET_UNAUTHORIZED });
      dispatch(uiActions.stopLoading());
      return;
    }
    const response = await authApi.authorization(token);
    localStorage.setItem("token", response.token);
    const action = {
      type: actionTypes.FILL_USER_DATA,
      payload: response.user,
    };
    dispatch(action);
    dispatch(uiActions.stopLoading());
  },

  getOne: (id) => async (dispatch) => {
    const token = localStorage.getItem("token");
    const response = await usersApi.getOne(id, token);
    const action = {
      type: actionTypes.GET_USER_DATA,
      payload: { ...response },
    };
    dispatch(action);
  },

  getProfile: () => async (dispatch) => {
    dispatch(uiActions.startLoading());
    const token = localStorage.getItem("token");
    const { data } = await usersApi.getProfile(token);
    const action = {
      type: actionTypes.FILL_USER_DATA,
      payload: { ...data },
    };
    dispatch(action);
    dispatch(uiActions.stopLoading());
  },

  getAll: (params = {}) => async (dispatch) => {
    const token = localStorage.getItem("token");
    const response = await usersApi.getAll(params, token);
    const action = {
      type: actionTypes.GET_ALL,
      payload: response,
    };
    dispatch(action);
  },
};
