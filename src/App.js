import "bootstrap/dist/css/bootstrap.min.css";
import "./App.css";
import React from "react";
import { BrowserRouter } from "react-router-dom";
import { store } from "./store";
import { Provider } from "react-redux";
import Routes from "../src/routes";
import { Header, Footer, CurrentUserChecker } from "../src/components";
import { Container } from "react-bootstrap";

function App() {
  return (
    <Provider store={store}>
      <CurrentUserChecker>
        <BrowserRouter>
          <Header />
          <main className="main">
            <Container>
              <Routes />
            </Container>
          </main>
          <Footer />
        </BrowserRouter>
      </CurrentUserChecker>
    </Provider>
  );
}

export default App;
