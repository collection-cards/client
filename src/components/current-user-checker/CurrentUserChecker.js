import { useEffect } from "react";
import { useDispatch } from "react-redux";
import { userActions } from "../../store/actions";

export const CurrentUserChecker = ({ children }) => {
  const dispatch = useDispatch();

  useEffect(() => {
    dispatch(userActions.authorization());
  }, [dispatch]);

  return children;
};
