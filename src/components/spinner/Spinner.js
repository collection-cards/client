import "./Spinner.css";
import React from "react";

export const Spinner = () => {
  return (
    <div className="spinner-wrapper">
      <div className="spinner" />
    </div>
  );
};
