import "./Footer.css";
import React from "react";
import { Link } from "react-router-dom";
import { book } from "../../routes/book";

export const Footer = () => {
  return (
    <footer className="footer">
      <div className="container footer__content">
        <Link to={book.pages.home} className="nav-link">
          Go to home
        </Link>
      </div>
    </footer>
  );
};
