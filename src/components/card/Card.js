import "./Card.css";
import React from "react";
import { Link } from "react-router-dom";
import { Card } from "react-bootstrap";
import { book } from "../../routes/book";

export const CustomCard = (props) => {
  const { id, name, status, gender, image, price } = props;

  return (
    <Card bg="secondary" text="light" className="custom-card">
      <Card.Img variant="top" src={image} />
      <Card.Body className="card-content">
        <Card.Title>
          <Link
            to={`${book.pages.cards}/${id}`}
            className="text-content__link"
          >
            <h2>{name}</h2>
          </Link>
        </Card.Title>
        <Card.Text>
          <span className="text-content__title">
            Price: <span className="text-content__info">{price}</span>
          </span>
          <span className="text-content__title">
            Status: <span className="text-content__info">{status}</span>
          </span>
          <span className="text-content__title">
            Gender: <span className="text-content__info">{gender}</span>
          </span>
        </Card.Text>
      </Card.Body>
    </Card>
  );
};
