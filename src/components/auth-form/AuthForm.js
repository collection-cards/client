import "./AuthForm.css";
import React from "react";
import { useFormik } from "formik";
import * as yup from "yup";
import { useDispatch, useSelector } from "react-redux";
import { Button, Form } from "react-bootstrap";
import { userActions } from "../../store/actions";
import { Spinner } from "../../components";

export const AuthForm = ({ isLogin = false }) => {
  const dispatch = useDispatch();
  const isLoading = useSelector(({ ui }) => ui.isLoading);
  const { errors, touched, ...formik } = useFormik({
    initialValues: {
      email: "",
      password: "",
      confirmPassword: "",
    },
    validationSchema: yup.object().shape({
      email: yup.string().email("Invalid email address").required("Required"),
      password: yup.string().required("Required"),
      confirmPassword: isLogin
        ? null
        : yup
            .string()
            .oneOf([yup.ref("password")], "Passwords mismatch")
            .required("Required"),
    }),
    onSubmit: (values) => {
      const { confirmPassword, ...data } = values;
      if (isLogin) {
        dispatch(userActions.login(data));
        return;
      }
      dispatch(userActions.create(data));
    },
  });

  if (isLoading) {
    return <Spinner />;
  }

  return (
    <Form className="form" onSubmit={formik.handleSubmit}>
      <Form.Group>
        <Form.Label>Email Adress</Form.Label>
        <Form.Control
          type="email"
          name="email"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.email}
          placeholder="Enter email"
        />
        <Form.Text className="text-muted">
          {errors.email && touched.email && (
            <div className="error">{errors.email}</div>
          )}
          We'll never share your email with anyone else
        </Form.Text>
      </Form.Group>
      <Form.Group>
        <Form.Label>Password</Form.Label>
        <Form.Control
          type="password"
          name="password"
          onChange={formik.handleChange}
          onBlur={formik.handleBlur}
          value={formik.values.password}
          placeholder="Enter password"
        />
        <Form.Text className="text-muted">
          {errors.password && touched.password && (
            <div className="error">{errors.password}</div>
          )}
        </Form.Text>
      </Form.Group>
      {!isLogin && (
        <Form.Group>
          <Form.Label>Confirm password</Form.Label>
          <Form.Control
            type="password"
            name="confirmPassword"
            onChange={formik.handleChange}
            onBlur={formik.handleBlur}
            value={formik.values.confirmPassword}
            placeholder="Confirm password"
          />
          <Form.Text className="text-muted">
            {errors.confirmPassword && touched.confirmPassword && (
              <div className="error">{errors.confirmPassword}</div>
            )}
          </Form.Text>
        </Form.Group>
      )}
      <Button
        variant="primary"
        type="submit"
        disabled={!formik.dirty || !formik.isValid}
        onClick={formik.handleSubmit}
      >
        Submit
      </Button>
    </Form>
  );
};
