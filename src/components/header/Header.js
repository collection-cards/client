import "./Header.css";
import React from "react";
import { useDispatch, useSelector } from "react-redux";
import { NavLink } from "react-router-dom";
import { Navbar, Nav, Container, Button } from "react-bootstrap";
import { book } from "../../routes/book";
import { userActions } from "../../store/actions";

const createNavLink = (url, text) => (
  <NavLink
    to={url}
    exact
    className="mr-2 nav-link"
    activeClassName="active-link"
  >
    {text}
  </NavLink>
);

export const Header = () => {
  const dispatch = useDispatch();
  const { isLoggedIn } = useSelector(({ user }) => user);
  const { home, cards, signIn, signUp, profile } = book.pages;

  const handleLoguot = () => {
    dispatch(userActions.logout());
  };

  const elements = isLoggedIn ? (
    <Nav>
      {createNavLink(profile, "Profile")}
      <Button onClick={handleLoguot}>Logout</Button>
    </Nav>
  ) : (
    <Nav>
      {createNavLink(signIn, "Sign In")}
      {createNavLink(signUp, "Sign Up")}
    </Nav>
  );

  return (
    <header className="header">
      <Navbar collapseOnSelect expand="lg" bg="dark" variant="darck">
        <Container>
          <Navbar.Brand className="logo">Collection Cards</Navbar.Brand>
          <Navbar.Toggle aria-controls="responsive-navbar-nav" />
          <Navbar.Collapse id="responsive-navbar-nav">
            <Nav className="mr-auto">
              {createNavLink(home, "Home")}
              {createNavLink(cards, "Catalog")}
            </Nav>
            {isLoggedIn === null ? null : elements}
          </Navbar.Collapse>
        </Container>
      </Navbar>
    </header>
  );
};
