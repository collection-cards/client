import "./Pagination.css";
import React from "react";
import ReactPaginate from "react-paginate";

export const CustomPagination = ({ pageCount, currentPage, onPageChange }) => {
  return (
    <ReactPaginate
      containerClassName="pagination"
      pageLinkClassName="page-link"
      previousLinkClassName="page-link"
      nextLinkClassName="page-link"
      breakLinkClassName="page-link"
      activeLinkClassName="active disabled"
      previousLabel={"«"}
      nextLabel={"»"}
      breakLabel={`...`}
      pageCount={pageCount}
      marginPagesDisplayed={4}
      onPageChange={onPageChange}
      initialPage={currentPage}
      disableInitialCallback={true}
    />
  );
};
