import React from "react";
import { Switch, Route } from "react-router-dom";
import { useSelector } from "react-redux";
import { Home, Catalog, Element } from "../pages";
import { book } from "./book";
import { PrivateRoutes } from "./private";
import { PablicRoutes } from "./pablic";
import { Spinner } from "../components";

const Routes = () => {
  console.log("Routes");
  const { home, cards } = book.pages;
  const { isLoggedIn } = useSelector(({ user }) => user);

  if (isLoggedIn === null) {
    return <Spinner />;
  }

  return (
    <Switch>
      <Route path={home} component={Home} exact />
      <Route path={cards} component={Catalog} exact />
      <Route path={cards + "/:elementId"} component={Element} />
      {isLoggedIn ? <PrivateRoutes /> : <PablicRoutes />}
    </Switch>
  );
};

export default Routes;
