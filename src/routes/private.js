import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { Profile } from "../pages";
import { book } from "./book";

export const PrivateRoutes = () => {
  const { profile, home } = book.pages;

  return (
    <Switch>
      <Route path={profile} component={Profile} />
      <Redirect from="*" to={home} />
    </Switch>
  );
};
