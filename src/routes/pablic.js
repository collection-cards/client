import React from "react";
import { Route, Redirect, Switch } from "react-router-dom";
import { SignIn, SignUp } from "../pages";
import { book } from "./book";

export const PablicRoutes = () => {
  const { signIn, signUp } = book.pages;

  return (
    <Switch>
      <Route path={signIn} component={SignIn} />
      <Route path={signUp} component={SignUp} />
      <Redirect from="*" to={signIn} />
    </Switch>
  );
};
