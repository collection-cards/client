export const book = {
  pages: {
    home: "/",
    cards: "/cards",
    signIn: "/sign-in",
    signUp: "/sign-up",
    profile: "/profile",
  },
};
